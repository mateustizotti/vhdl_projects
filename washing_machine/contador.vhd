library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity contador is
    port (
    clock: in std_logic; 
	 enable: in std_logic;
	 flag: out std_logic
    );
end contador; 

architecture contador of contador is

	signal flag_sig: std_logic := '0';
	
begin

Count: process (clock, enable)
	variable count : natural := 0;
begin
	IF (clock'event and clock = '1' and enable = '1') THEN
		IF (count <= 10) THEN
			count := count + 1;
		ELSE flag_sig <= '1';
		END IF;
	END IF;
	 
end process;
flag <= flag_sig;

end contador; 
