library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_unsigned.all ;

entity estados is
	port (
			washing_type 		: IN std_logic_vector(1 downto 0); 		
			level_input	     	: IN std_logic_vector(1 downto 0); 		
			level_sensor        : IN std_logic_vector(1 downto 0); 
			ligar				: IN std_logic;
			extra_dry    		: IN std_logic; 						
			heater	     		: IN std_logic; 						
			clock		  		: IN std_logic; 		
			washing_process		: out std_logic_vector(2 downto 0)
			);   
end estados;

architecture estados of estados is
    
	TYPE State_type IS (parada, encher_fria, encher_quente, lavagem, drenagem, centrifuga);
	SIGNAL FSM 		 	  : State_type;
	SIGNAL s_process 	  : std_logic_vector(2 downto 0) := "000";
	SIGNAL timer_done 	  : std_logic := '0';
	SIGNAL counter_enable : std_logic := '0';
	SIGNAL done           : std_logic := '0';
	
begin

	counter: entity work.contador
			port map (
				clock => clock,
				enable => counter_enable,
				flag => timer_done
			);
	
	process(washing_type, level_input, level_sensor, extra_dry, heater, clock, ligar, FSM)
	begin
		IF rising_edge(clock) and (ligar = '1') THEN
			CASE FSM IS
				WHEN parada =>
					IF (done = '0') THEN
						IF (heater = '0')
							THEN FSM <= encher_fria;
						ELSE FSM <= encher_quente;
						END IF;
					END IF;
				WHEN encher_fria =>
					IF level_sensor = level_input
						THEN FSM <= lavagem;
					ELSE FSM <= encher_fria;
					END IF;
				WHEN encher_quente =>
				IF level_sensor = level_input
					THEN FSM <= lavagem;
				ELSE FSM <= encher_quente;
				END IF;
				WHEN lavagem => 
					CASE washing_type IS
						WHEN "00" =>
							counter_enable <= '1';
							if timer_done='1'
								THEN FSM <= drenagem; 
								ELSE FSM <= lavagem;
							END IF;
						WHEN "01" =>
							counter_enable <= '1';
							if timer_done='1'
								THEN FSM <= drenagem; 
								ELSE FSM <= lavagem;
							END IF;
						WHEN "10" =>
							counter_enable <= '1';
							if timer_done='1'
								THEN FSM <= drenagem; 
								ELSE FSM <= lavagem;
							END IF;
						WHEN others =>
							done <= '1';
							FSM <= parada;
					END CASE;
--				END IF; 
				
				WHEN drenagem =>
					IF level_sensor = "00"
						THEN FSM <= centrifuga;
					ELSE FSM <= drenagem;
					END IF;
				WHEN centrifuga =>
					IF extra_dry = '1' THEN
						counter_enable <= '1';
						if timer_done = '1' then
							done <= '1';
							FSM <= parada;
						end if;
						
					ELSIF extra_dry = '0' THEN
						counter_enable <= '1';
						if timer_done = '1' then
							done <= '1';
							FSM <= parada;
						end if;
					END IF;
				WHEN others =>
					done <= '1';
					FSM <= parada;
			END CASE;
			CASE FSM IS
				WHEN parada => washing_process <= "000";
				WHEN encher_fria => washing_process <= "001";
				WHEN encher_quente => washing_process <= "010";
				WHEN lavagem => washing_process <= "011";
				WHEN drenagem => washing_process <= "100";
				WHEN centrifuga => washing_process <= "101";
			END CASE;
		end if;
	end process;
end estados;