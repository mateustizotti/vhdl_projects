library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_unsigned.all;

entity washing_machine is
	port (
			washing_type 		: IN std_logic_vector(1 downto 0); 		
			level_input	     	: IN std_logic_vector(1 downto 0); 		
			level_sensor        : IN std_logic_vector(1 downto 0);
			ligar 				: IN std_logic;
			extra_dry    		: IN std_logic; 						
			heater	     		: IN std_logic; 						
			clock		  		: IN std_logic; 		
			tl_motobomba        : OUT std_logic;
			tl_drenagem   		: OUT std_logic;
			tl_motor   			: OUT std_logic;
			tl_heater	  		: OUT std_logic
			);   
end washing_machine;

architecture washing_machine of washing_machine is

	component estados
	PORT
		(
         washing_type 		: IN std_logic_vector(1 downto 0); 		
			level_input	    : IN std_logic_vector(1 downto 0); 		
			level_sensor    : IN std_logic_vector(1 downto 0); 	
			ligar			: IN std_logic;
			extra_dry       : IN std_logic; 						
			heater	        : IN std_logic; 
			clock		  	: IN std_logic; 		
			washing_process	: out std_logic_vector(2 downto 0)
		);
	end component;
    
    component saidas
	PORT
		(
         state   			: IN std_logic_vector(2 downto 0);
			washing_type    : IN std_logic_vector(1 downto 0); 	
         s_motobomba   		: OUT std_logic;
			s_drenagem   	: OUT std_logic;
			s_motor   		: OUT std_logic;
			s_heater	    : OUT std_logic
		);
	end component;

	SIGNAL estado  : std_logic_vector(2 downto 0) := "000";
	
begin
	
    estates: estados
		PORT MAP
		(
		 washing_type,
         level_input,
         level_sensor,
		 ligar,
         extra_dry,
         heater,
         clock,
    	 estado
		);
    outs: saidas
		PORT MAP
		(
		 estado,
         washing_type,
         tl_motobomba,
         tl_drenagem,
         tl_motor,
         tl_heater
		);
	
end washing_machine;