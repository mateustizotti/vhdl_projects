library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_unsigned.all ;

entity saidas is
    port (
        state               : IN std_logic_vector(2 downto 0);
        washing_type        : IN std_logic_vector(1 downto 0);  
        s_motobomba         : OUT std_logic;
        s_drenagem          : OUT std_logic;
        s_motor             : OUT std_logic;
        s_heater            : OUT std_logic
        );   
end saidas;

architecture saidas of saidas is


begin

    process (state)
    begin
    case state is
        when "000" =>
            s_motobomba <= '0';
            s_drenagem <= '0';
            s_motor <= '0';
            s_heater <= '0';
        when "001" =>
            s_motobomba <= '1';
            s_drenagem <= '0';
            s_motor <= '0';
            s_heater <= '0';
        when "010" =>
            s_motobomba <= '1';
            s_drenagem <= '0';
            s_motor <= '0';
            s_heater <= '1';
        when "011" =>
            s_motobomba <= '0';
            s_drenagem <= '0';
            s_motor <= '1';
            s_heater <= '0';
        when "100" =>
            s_motobomba <= '0';
            s_drenagem <= '1';
            s_motor <= '0';
            s_heater <= '0';
        when "101" =>
            s_motobomba <= '0';
            s_drenagem <= '0';
            s_motor <= '1';
            s_heater <= '0';
            when others =>
            s_motobomba <= '0';
            s_drenagem <= '0';
            s_motor <= '0';
            s_heater <= '0';
        end case;
    end process;
    
end saidas;