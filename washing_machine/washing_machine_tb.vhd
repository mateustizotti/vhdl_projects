LIBRARY ieee;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_unsigned.all;
 
ENTITY washing_machine_tb IS
END washing_machine_tb;
 
ARCHITECTURE washing_machine_tb OF washing_machine_tb IS 
 
    COMPONENT washing_machine
    PORT(
				washing_type		  : IN std_logic_vector(1 downto 0);
				level_input 		  : IN std_logic_vector(1 downto 0);
				level_sensor  		  : IN std_logic_vector(1 downto 0);
				ligar					  : In std_logic;
				extra_dry 			  : IN std_logic; 
				heater  				  : IN std_logic;
				clock 		  		  : IN std_logic;
				tl_motobomba		  : OUT std_logic;
				tl_drenagem			  : OUT std_logic;
				tl_motor				  : OUT std_logic;
				tl_heater			  : OUT std_logic
		);
    END COMPONENT;
    
   signal washing_type   : std_logic_vector(1 downto 0) := (others => '0');
   signal level_input    : std_logic_vector(1 downto 0) := (others => '0');
   signal level_sensor  : std_logic_vector(1 downto 0) := (others => '0');
   signal ligar		 : std_logic := '0';
   signal extra_dry  	 : std_logic := '0';
   signal heater 		 : std_logic := '0';
   signal clock		 : std_logic := '0';

   signal tl_motobomba   : std_logic := '0';
   signal tl_drenagem   : std_logic := '0';
   signal tl_motor 	 : std_logic := '0';
   signal tl_heater	 : std_logic := '0';

   constant tempo_clock 	: time := 24 ns;
   constant tempo_processo : time := 27 ns;

BEGIN
 
   uut: washing_machine 
		PORT MAP (
          	 washing_type => washing_type,
			 level_input  => level_input,
			 level_sensor => level_sensor,
			 ligar		  => ligar,
			 extra_dry    => extra_dry,
			 heater       => heater,
			 clock        => clock,
			 tl_motobomba => tl_motobomba,
			 tl_drenagem  => tl_drenagem,
			 tl_motor     => tl_motor,
			 tl_heater    => tl_heater
        );
		  
	clock_proc: process
	begin
		clock <= '0';
		wait for tempo_clock/2;
		clock <= '1';
		wait for tempo_clock/2;
	end process;

   stim_proc: process
   begin		
 		washing_type <= "10";
		level_input  <= "11";
		level_sensor <= "00";
		ligar        <= '1';
		extra_dry    <= '1';
		heater       <= '0';
			wait for tempo_processo;	
		washing_type <= "10";
		level_input  <= "11";
		level_sensor <= "01";
		ligar        <= '1';
		extra_dry    <= '1';
		heater       <= '0';
			wait for tempo_processo;	
		washing_type <= "10";
		level_input  <= "11";
		level_sensor <= "10";
		ligar        <= '1';
		extra_dry    <= '1';
		heater       <= '0';
			wait for tempo_processo;	
		washing_type <= "10";
		level_input  <= "11";
		level_sensor <= "11";
		ligar        <= '1';
		extra_dry    <= '1';
		heater       <= '0';
			wait for 370 ns;
		washing_type <= "10";
		level_input  <= "11";
		level_sensor <= "10";
		ligar        <= '1';
		extra_dry    <= '1';
		heater       <= '0';
			wait for 200 ns;
		washing_type <= "10";
		level_input  <= "11";
		level_sensor <= "01";
		ligar        <= '1';
		extra_dry    <= '1';
		heater       <= '0';
			wait for 100 ns;
		washing_type <= "10";
		level_input  <= "11";
		level_sensor <= "00";
		ligar        <= '1';
		extra_dry    <= '1';
		heater       <= '0';
			wait for 100 ns;
      wait;
   end process;
END;