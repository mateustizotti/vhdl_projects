library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity projeto_ga is
	port (
		level_high		: IN std_logic;
		level_low		: IN std_logic;
		temp_hot		: IN std_logic;
		temp_cold		: IN std_logic;
		clk   			: IN std_logic;
		valve_in		: OUT std_logic;
		valve_out		: OUT std_logic;
		heater			: OUT std_logic;
		alarm			: OUT std_logic
		);
end projeto_ga;

architecture projeto_ga of projeto_ga is
  
	signal state 		: std_logic_vector(2 downto 0) := "000";
	signal valve_in_s	: std_logic := '0';
	signal valve_out_s	: std_logic := '0';
	signal heater_s		: std_logic := '0';
	signal alarm_s		: std_logic := '0';

begin

	valve_in <= valve_in_s;
	valve_out <= valve_out_s;
	heater <= heater_s;
	alarm <= alarm_s;

	process(clk, level_high, level_low, temp_hot, temp_cold, state)
        begin
            if (rising_edge(clk))then
                state(0) <= (level_low and not temp_hot and not temp_cold) or (level_high and level_low and not temp_hot); 
                state(1) <= (level_high and not level_low) or (level_high and temp_cold) or temp_hot;
                state(2) <= (not level_high and temp_cold) or (not level_low and temp_cold) or (temp_hot and temp_cold) or (level_high and not temp_cold);
            end if;

         valve_in_s  <= (not state(0) and not state(1)) or (not state(0) and not state(2)) or (not state(1) and not state(2));
			valve_out_s <= state(0) and not state(1);
			heater_s    <= (not state(0) and not state(1) and state(2)) or (state(0) and state(1) and not state(2));
			alarm_s     <= (not state(0) and state(1)) or (state(1) and state(2));
        end process;
end projeto_ga;
