library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity sum1 is
	port (
		a		: IN std_logic;
		b		: IN std_logic;
      cin   : IN std_logic;
      cout  : OUT std_logic;
		out1	: OUT std_logic
		);
end sum1;

architecture sum1 of sum1 is

begin
    out1 <= a xor b xor cin;
    cout <= (a and b) or (a and cin) or (b and cin);

end sum1;
