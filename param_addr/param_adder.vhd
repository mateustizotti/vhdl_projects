library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

entity param_addr is
    generic (
        NUM_BITS   : INTEGER := 4
    );
	port (
		y		   : IN std_logic_vector(NUM_BITS - 1 downto 0);
		x		   : IN std_logic_vector(NUM_BITS - 1 downto 0);
      cin_tl   : IN std_logic;
      cout_tl  : OUT std_logic;
		out_tl	: OUT std_logic_vector(NUM_BITS - 1 downto 0)
		);
end param_addr;

architecture param_addr of param_addr is

	component sum1
	PORT
		(
			a, b, cin:  IN std_logic;
			cout, out1: OUT std_logic
		);
	end component;

	signal aux_s: std_logic_vector(NUM_BITS - 1 downto 0) := (conv_std_logic_vector(0, NUM_BITS));

begin
    
    inst_param: for i in 0 to NUM_BITS - 1 generate
        def: if i = 0 generate
            inst_first_sum1: sum1
                PORT MAP
                (
                    x(i),
                    y(i),
                    cin_tl,
                    aux_s(i+1),
                    out_tl(i) 
                );
        end generate def;
        middle: if (i > 0) AND (i < NUM_BITS - 1) generate
            inst_middle_sum1: sum1
                PORT MAP
                (
                    x(i),
                    y(i),
                    aux_s(i),
                    aux_s(i+1),
                    out_tl(i) 
                );
        end generate middle;
        last: if i = NUM_BITS - 1 generate
            inst_last_sum1: sum1
                PORT MAP
                (
                    x(i),
                    y(i),
                    aux_s(i),
                    cout_tl,
                    out_tl(i) 
                );
        end generate last;
    end generate inst_param;
	
end param_addr;