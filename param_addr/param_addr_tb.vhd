library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_arith.all;
	use ieee.std_logic_unsigned.all;
	use ieee.numeric_std.all;

ENTITY addr_param_tb IS
	generic ( 
		NUM_BITS		: integer	:= 18
	);
END addr_param_tb;

ARCHITECTURE behavior OF addr_param_tb IS 

	COMPONENT addr_param
		GENERIC ( 
			NUM_BITS		: integer
			);

		PORT(
			y			: IN std_logic_vector(NUM_BITS - 1 downto 0);
			x			: IN std_logic_vector(NUM_BITS - 1 downto 0);
			cin_tl      : IN std_logic;
			cout_tl     : OUT std_logic; 
			out_tl    	: OUT std_logic_vector(NUM_BITS - 1 downto 0)
		);
	END COMPONENT;

	signal 		y 		: std_logic_vector(NUM_BITS-1 downto 0) := (others => '0');
	signal 		x 		: std_logic_vector(NUM_BITS-1 downto 0) := (others => '0');
	signal 		cin_tl 	: std_logic := '0';
	signal 		cout_tl : std_logic := '0';
	signal 		out_tl 	: std_logic_vector(NUM_BITS-1 downto 0) := (others => '0');
	
	constant tempo_processo : time := 14 ns;
	
	BEGIN

	uut: addr_param
		GENERIC MAP ( 
			NUM_BITS => NUM_BITS 
		)	
		PORT MAP (
			x => x,
			y => y,
			cin_tl => cin_tl,
			cout_tl => cout_tl,
			out_tl => out_tl
		);

	stim_proc: process
	begin
		cin_tl <= '0';
		 x <= conv_std_logic_vector(-7, NUM_BITS);
		 y <= conv_std_logic_vector(2, NUM_BITS);
		 wait for tempo_processo;
		 
		 x <= conv_std_logic_vector(-45, NUM_BITS);
		 y <= conv_std_logic_vector(-20, NUM_BITS);
		 wait for tempo_processo;
		 
		 x <= conv_std_logic_vector(12, NUM_BITS);
		 y <= conv_std_logic_vector(10, NUM_BITS);
		 wait for tempo_processo;
		
		 x <= conv_std_logic_vector(55, NUM_BITS);
		 y <= conv_std_logic_vector(-7, NUM_BITS);
		 wait for tempo_processo;
		 for index in 0 to 99 loop 
		 x <= x + conv_std_logic_vector(1,NUM_BITS); 
		 y <= y + conv_std_logic_vector(1,NUM_BITS);
		 end loop; 
		 wait;
	end process;
END;