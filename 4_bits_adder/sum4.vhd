library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

entity sum4 is
	port (
		y		   : IN std_logic_vector(3 downto 0);
		x		   : IN std_logic_vector(3 downto 0);
      cin_tl     : IN std_logic;
      cout_tl    : OUT std_logic;
		out_tl	: OUT std_logic_vector(3 downto 0)
		);
end sum4;

architecture sum4 of sum4 is

	component sum1
	PORT
		(
			a, b, cin:  IN std_logic;
			cout, out1: OUT std_logic
		);
	end component;

	signal aux_s: std_logic_vector(3 downto 0) := (conv_std_logic_vector(0, 4));

begin
    
	x0: sum1
		PORT MAP
		(
			x(0),
			y(0),
			cin_tl,
			aux_s(1),
			out_tl(0) 
		);
	x1: sum1
		PORT MAP
		(
			x(1),
			y(1),
			aux_s(1),
			aux_s(2),
			out_tl(1) 
		);
	x2: sum1
		PORT MAP
		(
			x(2),
			y(2),
			aux_s(2),
			aux_s(3),
			out_tl(2)
		);
	x3: sum1
		PORT MAP
		(
			x(3),
			y(3),
			aux_s(3),
			cout_tl,
			out_tl(3)
		);

end sum4;
