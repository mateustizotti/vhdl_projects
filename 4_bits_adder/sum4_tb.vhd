
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY sum4_tb IS
END sum4_tb;
 
ARCHITECTURE sum4_tb OF sum4_tb IS 
 
    COMPONENT sum4
    PORT(
				y	: IN std_logic_vector(3 downto 0);
				x 	: IN std_logic_vector(3 downto 0);
				cin_tl  : IN std_logic;
				cout_tl : OUT std_logic; 
				out_tl  : OUT std_logic_vector(3 downto 0)
		);
    END COMPONENT;
    
   signal y : std_logic_vector(3 downto 0) := "0000";
   signal x : std_logic_vector(3 downto 0) := "0000";
   signal cin_tl : std_logic := '0';

   signal out_tl : std_logic_vector(3 downto 0) := (others => '0');
	signal cout_tl: std_logic :='0';

   constant tempo_processo : time := 27 ns;

BEGIN
 
   uut: sum4 
		PORT MAP (
          y => y,
          x => x,
          cin_tl => cin_tl,
          cout_tl => cout_tl,
          out_tl => out_tl
        );

   stim_proc: process
   begin		
 		x <= "0001";
		y <= "0101";
		cin_tl <= '0';
      wait for tempo_processo;
		x <= "1010";
		y <= "0111";
		cin_tl <= '1';
      wait for tempo_processo;	
		x <= "0111";
		y <= "0010";
		cin_tl <= '1';
      wait for tempo_processo;	
		x <= "1111";
		y <= "1111";
		cin_tl <= '1';
      wait for tempo_processo;				
      wait;
   end process;

END;
