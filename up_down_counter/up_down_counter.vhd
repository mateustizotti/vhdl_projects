library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity up_down_counter is
	port (
		toggle	: IN std_logic;
		clk		: IN std_logic;
		count		: OUT std_logic_vector (3 downto 0)
		);
end up_down_counter;

architecture up_down_counter of up_down_counter is

signal c: std_logic_vector(3 downto 0) := "0000";

begin
	
	process(clk, toggle)
	begin
		if (rising_edge(clk))then
			if (toggle = '1')then
				c <= c + 1;
			elsif (toggle = '0')then
				c <= c - 1;
			end if;
		end if;
	end process;
count <= c;
	
end up_down_counter;

