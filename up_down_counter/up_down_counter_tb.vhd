LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY up_down_counter_tb IS
END up_down_counter_tb;
 
ARCHITECTURE behavior OF up_down_counter_tb IS 
 
    COMPONENT up_down_counter
    PORT(
         toggle : IN  std_logic;
         clk 	 : IN  std_logic;
         count  : OUT std_logic_vector(3 downto 0)
        );
    END COMPONENT;
	 
   signal toggle : std_logic := '0';
   signal clk    : std_logic := '0';
   signal count  : std_logic_vector(3 downto 0);
 
BEGIN

   uut: up_down_counter PORT MAP (
          toggle => toggle,
          clk => clk,
          count => count
        );

   clk_process :process
   begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
   end process;
 
   stim_proc: process
   begin		
      toggle <= '1';
			wait for 60 ns;
		toggle <= '0';
			wait for 60 ns;
      wait;
   end process;
END;

