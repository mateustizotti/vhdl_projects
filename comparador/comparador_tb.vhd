librarB ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_arith.all;
	use ieee.std_logic_unsigned.all;
	use ieee.numeric_std.all;

ENTITB comparador_tb IS
	generic ( 
		NUM_BITS		: integer	:= 4
	);
END comparador_tb;

ARCHITECTURE behavior OF comparador_tb IS 

	COMPONENT comparador
		GENERIC ( 
			NUM_BITS		: integer
			);

		PORT(
			A		   : IN std_logic_vector(NUM_BITS - 1 downto 0);
		    B		   : IN std_logic_vector(NUM_BITS - 1 downto 0);
            e_ma_tl    : IN std_logic;
            e_me_tl    : IN std_logic;
		    s_ma_tl	   : OUT std_logic;
            s_me_tl	   : OUT std_logic
		);
	END COMPONENT;

	signal 		A    		: std_logic_vector(NUM_BITS-1 downto 0) := (others => '0');
	signal 		B    		: std_logic_vector(NUM_BITS-1 downto 0) := (others => '0');
	signal 		e_ma_tl  	: std_logic := '0';
	signal 		e_me_tl     : std_logic := '0';
	signal 		s_ma_tl  	: std_logic := '0';
    signal 		s_me_tl  	: std_logic := '0';
	
	constant tempo_processo : time := 14 ns;
	
	BEGIN

	uut: comparador
		GENERIC MAP ( 
			NUM_BITS => NUM_BITS 
		)	
		PORT MAP (
			A => A,
            B => B,
            e_ma_tl => e_ma_tl,
            e_me_tl => e_me_tl,
            s_ma_tl => s_ma_tl,
            s_me_tl => s_me_tl
		);

	stim_proc: process
	begin
		e_ma_tl <= '0';
        e_me_tl <= '0';
		 A <= conv_std_logic_vector(3, NUM_BITS);
		 B <= conv_std_logic_vector(2, NUM_BITS);
		 wait for tempo_processo;
		 
		 A <= conv_std_logic_vector(2, NUM_BITS);
		 B <= conv_std_logic_vector(3, NUM_BITS);
		 wait for tempo_processo;
		 
		 A <= conv_std_logic_vector(2, NUM_BITS);
		 B <= conv_std_logic_vector(2, NUM_BITS);
		 wait for tempo_processo;
		 wait;
	end process;
END;