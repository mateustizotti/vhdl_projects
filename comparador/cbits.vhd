library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity cbits is
	port (
		a		: IN std_logic;
		b		: IN std_logic;
        e_ma    : IN std_logic;
        e_me    : IN std_logic;
		s_ma	: OUT std_logic;
        s_me	: OUT std_logic
		);
end cbits;

architecture cbits of cbits is

begin
    s_ma <= (e_ma and not e_me) or (not e_me and a and not b);
    s_me <= (not e_ma and e_me) or (not e_ma and not a and b);

end cbits;
