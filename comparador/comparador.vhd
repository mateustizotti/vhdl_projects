library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

entity comparador is
    generic (
        NUM_BITS   : INTEGER := 4
    );
	port (
		A		   : IN std_logic_vector(NUM_BITS - 1 downto 0);
		B		   : IN std_logic_vector(NUM_BITS - 1 downto 0);
        e_ma_tl    : IN std_logic;
        e_me_tl    : IN std_logic;
		s_ma_tl	   : OUT std_logic;
        s_me_tl	   : OUT std_logic
		);
end comparador;

architecture comparador of comparador is

	component cbits
	PORT
		(
            a       : IN std_logic;
            b       : IN std_logic;
            e_ma    : IN std_logic;
            e_me    : IN std_logic;
            s_ma    : OUT std_logic;
            s_me    : OUT std_logic
		);
	end component;

	signal aux_s_ma: std_logic_vector(NUM_BITS - 1 downto 0) := (conv_std_logic_vector(0, NUM_BITS));
    signal aux_s_me: std_logic_vector(NUM_BITS - 1 downto 0) := (conv_std_logic_vector(0, NUM_BITS)) ;

begin
    
    inst_param: for i in NUM_BITS - 1 downto 0 generate
        def: if i = NUM_BITS - 1 generate
            inst_first_sum1: cbits
                PORT MAP
                (
                    A(i),
                    B(i),
                    e_ma_tl,
                    e_me_tl,
                    aux_s_ma(i-1),
                    aux_s_me(i-1) 
                );
        end generate def;
        middle: if (i > 0) AND (i < NUM_BITS - 1) generate
            inst_middle_sum1: cbits
                PORT MAP
                (
                    A(i),
                    B(i),
                    aux_e_ma(i),
                    aux_e_me(i), 
                    aux_s_ma(i-1),
                    aux_s_ma(i-1) 
                );
        end generate middle;
        last: if i = 0 generate
            inst_last_sum1: cbits
                PORT MAP
                (
                    A(i),
                    B(i),
                    aux_e_ma(i),
                    aux_e_me(i), 
                    s_ma_tl,
                    s_me_tl
                );
        end generate last;
    end generate inst_param;
	
end comparador;